import os,sys
migration_f=sys.argv[1]
rep=int(sys.argv[2])
genetree_file=sys.argv[3]
num_taxa=int(sys.argv[4])
taxon=int(sys.argv[5])
truenet_f=sys.argv[6]
network_f=sys.argv[7]


scripts='/mnt/home/alaminmd/research/scripts'

out_file='../result_calgtprob_'+network_f+'_'+str(taxon)+'.txt'

def extract_calgtprob(filename):
    prob=0.0
    with open(filename,'r') as f:
        for line in f:
            if (line.startswith('Total log probability')):
                prob=float(line.split(': ')[1].split('\n')[0])                
    return prob 


pruned_net='without_'+str(taxon)+'.net'
#cmd='python '+scripts+'/deep_network_drop_taxon.py '+migration_f+' '+str(num_taxa)+'.txt'+ ' '+str(taxon)+' '+str(rep)+ ' trees_without_bl_'+str(num_taxa)+'.txt'
cmd='python '+scripts+'/drop_taxon_network_base.py '+network_f+' '+str(taxon)
os.system(cmd)
temp_nets='tempnets_'+str(taxon)+'.net'
cmd='python '+scripts+'/add_taxon_network.py '+pruned_net+' '+str(num_taxa)+' '+str(taxon)+' > '+temp_nets

os.system(cmd)
all_new_nets=[]
all_nets_probs=[]
with open(temp_nets,'r') as tnf:
    for line in tnf:
        if(line!='\n'):
            all_new_nets.append(line)
            new_net='newnet_'+str(taxon)+'.net'
            nexus_f='nexus_'+str(taxon)+'.nexus'
            result_f='calgtp_res'+str(taxon)+'.txt'
            with open(new_net,'w') as nnf:
                nnf.write(line)
            cmd= 'python '+scripts+'/create_nexus_calgtprob.py '+new_net+ ' '+genetree_file+ ' '+nexus_f+' '+str(num_taxa)
            os.system(cmd)
            cmd='java -jar /mnt/home/alaminmd/research/temp/test/PhyloNet.jar '+nexus_f+'>'+result_f
            os.system(cmd)
            prob=extract_calgtprob(result_f)
            all_nets_probs.append(prob)
            print(all_nets_probs)

largest_indx=0
largest_val=-9999999
for i in range(len(all_nets_probs)):
    if(all_nets_probs[i]>largest_val):
        largest_val=all_nets_probs[i]
        largest_indx=i
final_net_with_max_prob=all_new_nets[largest_indx]

print('%s %s'%(str(taxon),final_net_with_max_prob))
result_net='result_net_'+network_f+'_'+str(taxon)+'.net'
with open(result_net,'w') as rnf:
    rnf.write(final_net_with_max_prob)

cmd='echo q_'+str(taxon)+' >> '+out_file
os.system(cmd)

cmd='python '+scripts+'/compareNets.py '+ truenet_f+' '+result_net+' '+str(taxon)+' '+str(rep)+' 0 >> '+ out_file
os.system(cmd)        

pruned_inf=pruned_net+'_inf'
pruned_true=pruned_net+'_true'

cmd='mv '+pruned_net+' '+pruned_inf
os.system(cmd)

cmd='python '+scripts+'/drop_taxon_network_base.py '+truenet_f+' '+str(taxon)
os.system(cmd)

cmd='mv '+pruned_net+' '+pruned_true
os.system(cmd)

cmd='python '+scripts+'/compareNets.py '+ pruned_true+' '+pruned_inf+' '+str(taxon)+' '+str(rep)+' 0 >> '+ out_file
os.system(cmd)  

