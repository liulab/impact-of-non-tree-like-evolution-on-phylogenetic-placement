import os,sys
import random
script_path="/mnt/home/alaminmd/research/scripts"

ms_command_file=sys.argv[1]
rep=int(sys.argv[2])
num_taxa=sys.argv[3]
num_ret=sys.argv[4]
depth=sys.argv[5]
theta=sys.argv[6]
height=sys.argv[7]

ms_commands=[]
with open(ms_command_file,'r') as mscf:
    for line in mscf:
        ms_command=line.split('\n')[0]
        ms_commands.append(ms_command)

cmd='rm -r '+str(rep)
os.system(cmd)

replica_folder=str(rep)
cmd='mkdir '+replica_folder
os.system(cmd)

os.chdir(replica_folder)

cmd=ms_commands[rep-1]
os.system(cmd)

output_file=cmd.split('> ')[1] ##contains the file prefix infos
output_file_m=output_file+'_m'
cmd='python '+script_path+'/add_nonultra.py '+output_file+' 2.0 '+theta
os.system(cmd)

'''
### For inferring netwroks from true Gene trees ####

r_cmd='Rscript '+script_path+'/get_genetrees_one_step_rooting.r '+height+' '+num_taxa+' '+num_ret+' '+str(rep)+' '+theta+' '+'txt_m'
os.system(r_cmd)

new_genetree_file="rooted_genetrees_with_bl_"+height+"_"+num_taxa+"_"+num_ret+"_"+str(rep)+".txt_m"

nexus_f='nexus_rep_'+str(rep)+'.nexus'
cmd= 'python '+script_path+'/create_nexus_infernet.py '+new_genetree_file+ ' '+nexus_f+' '+num_taxa+' '+num_ret
os.system(cmd)

cmd='java -jar /mnt/home/alaminmd/research/temp/test/PhyloNet.jar '+nexus_f+'>infered_nets.net_m'
os.system(cmd)

### For inferring netwroks from true Gene trees ####
'''

seq_file=output_file.split('.txt')[0]+'.fasta'
msa_file=output_file.split('.txt')[0]+'_all_aln.fasta'

'''
indelible_cmd='python '+script_path+'/create_control_indelible.py '+script_path+'/ref_control.txt '+output_file_m+' '+depth
os.system(indelible_cmd)
cmd='indelible'
os.system(cmd)
'''

### For Estimated gene tree experiments #####

seq_gen_tree=output_file
#cmd='tail -n +4 '+output_file+' | grep -v // > '+seq_gen_tree
#os.system(cmd)
num_tree=0
with open(output_file,'r') as outf:
    for line in outf:
        if(line!='\n'):
            num_tree+=1
#print("The number of trees:#########  ")        
#print('%s\n'%str(num_tree))

#seq_file=output_file.split('.txt')[0]+'.fasta'
rd=random.randrange(12345, 999999, 15)
#print(seq_gen_tree)

seq_cmd='seq-gen -mHKY -l 1000 -s '+str(theta)+' '+seq_gen_tree+' > '+seq_file
os.system(seq_cmd)

'''
if(num_ret==0):
    #cmd='head -1 '+output_file+' > '+seq_gen_tree
    seq_cmd='seq-gen -mHKY -l 1000 -s 0.5 '+seq_gen_tree+' > '+seq_file
else:
    seq_cmd='seq-gen -mHKY -l 1000 -p '+str(num_tree)+' -s 0.5 -z '+str(rd)+' '+seq_gen_tree+' > '+seq_file
'''

concat_cmd='python '+script_path+'/concat_all_loci.py '+seq_file+' '+msa_file
os.system(concat_cmd)

estimated_gt_file=output_file+'_estimated'
cmd='rm '+estimated_gt_file
os.system(cmd)

for t in range(1,num_tree+1):
    cmd='head -'+str((int(num_taxa)+2)*t)+' '+seq_file+'|tail -'+str(int(num_taxa)+2)+' > temp_seq.fasta'
    os.system(cmd)

    cmd='FastTree -nt temp_seq.fasta >> '+estimated_gt_file
    os.system(cmd)
  
    cmd='rm temp_seq.fasta'
    os.system(cmd)




### For Estimated gene tree experiments #####

r_cmd='Rscript '+script_path+'/get_genetrees_one_step_rooting.r '+height+' '+num_taxa+' '+num_ret+' '+str(rep)+' '+theta+' '+'txt_estimated'
os.system(r_cmd)

new_genetree_file="rooted_genetrees_with_bl_"+height+"_"+num_taxa+"_"+num_ret+"_"+str(rep)+".txt_estimated"

nexus_f='nexus_rep_'+str(rep)+'.nexus'
cmd= 'python '+script_path+'/create_nexus_infernet.py '+new_genetree_file+ ' '+nexus_f+' '+num_taxa+' '+num_ret
os.system(cmd)

cmd='java -jar /mnt/home/alaminmd/research/temp/test/PhyloNet.jar '+nexus_f+'>infered_nets.net_estimated'
os.system(cmd)

