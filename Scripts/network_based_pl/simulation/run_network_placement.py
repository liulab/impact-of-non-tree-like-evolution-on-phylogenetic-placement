import os,sys

migration_f=sys.argv[1]
num_taxa=sys.argv[2]
height=sys.argv[3]
num_ret=sys.argv[4]
inferred_net_f=sys.argv[5]
extension=sys.argv[6]
scripts='/mnt/home/alaminmd/research/scripts/calgtp'
parent_dir=os.getcwd()
for rep in range(1,11):
    true_nets=[]
    net_file=str(num_taxa)+'.net'
    with open(net_file,'r') as nf:
        for line in nf:
            if(line!='\n'):
                true_nets.append(line)    
    
    repl_fold=str(rep)
    os.chdir(repl_fold)
    cmd='rm slurm*'
    #os.system(cmd)
    
    with open('truenet.net','w') as tnf:
        tnf.write(true_nets[rep-1])
    
    inf_nets={}
    with open(inferred_net_f,'r') as inn:
        save=False
        for line in inn:
            if(line.startswith('InferNetwork')):
                key=line.split(' ')[0]
            elif(line.startswith('Inferred Network #1:')):
                save=True
            elif(save):
                inf_nets[key]=line
                save=False
    
    network_f_ML='inf_net_ML.net_'+extension
    network_f_MPL='inf_net_MPL.net_'+extension
    network_f_MP='inf_net_MP.net_'+extension

    with open(network_f_ML,'w') as nfml:
        nfml.write(inf_nets['InferNetwork_ML'])

    with open(network_f_MPL,'w') as nfmpl:
        nfmpl.write(inf_nets['InferNetwork_MPL'])

    with open(network_f_MP,'w') as nfmp:
        nfmp.write(inf_nets['InferNetwork_MP'])
    
    gentree_f='rooted_genetrees_with_bl_'+height+'_'+str(num_taxa)+'_'+str(num_ret)+'_'+str(rep)+'.txt_'+extension
    for taxon in range(1,int(num_taxa)+1):
        cmd='sbatch -A liulab '+scripts+'/run_calgtp_parrallel.sh '+parent_dir+'/'+migration_f+' '+str(rep)+' '+gentree_f+' '+str(num_taxa)+' '+str(taxon)+' truenet.net'+' '+network_f_ML+' '+network_f_MPL+' '+network_f_MP
        os.system(cmd)
    
    os.chdir(parent_dir)    

