#!/bin/bash

rep=$1
num_taxa=$2
net_file=$3
migration_f=$4

cwd=`pwd`
out_apple="$cwd/result_apple_$rep.txt"
out_pplacer="$cwd/result_pplacer_$rep.txt"
out_epang="$cwd/result_epang_$rep.txt"
out_sepp="$cwd/result_sepp_$rep.txt"

rm $out_apple $out_pplacer $out_epang $out_sepp

#compare_path="/mnt/home/alaminmd/research/metagenomics/placement"
spliting_path="/mnt/home/alaminmd/research/scripts"
scripts="/mnt/home/alaminmd/research/scripts"

#tree_file="$num_taxa.txt"
#echo "$tree_file" 
#cd $taxa
cd $rep
head -$rep ../$net_file|tail -1 > true_net.net  

rm *.REF
#raxmlHPC-PTHREADS-AVX2 -f e -t reference_rax.tree -m GTRGAMMA -p 88 -n REF -s aln.fa -T 4
raxmlHPC -f e -t reference_rax.tree -m GTRGAMMA -p 88 -n REF -s aln.fa
python ~/research/scripts/fasta_phylip.py aln.fa aln.phy
fastme -dJ -i aln.phy -u RAxML_result.REF -o reference_rax_me.tree -T 1

Rscript  $scripts/outgroup_rooting.R reference_rax_me.tree $num_taxa reference_rax_me_rooted.tree  
Rscript  $scripts/outgroup_rooting.R RAxML_result.REF $num_taxa RAxML_result_rooted.REF  

outgrp=`expr $num_taxa + 1`
for ((i=1;i<=$num_taxa;i++))
do
cd $i

python $scripts/deep_network_drop_taxon.py "$cwd/$migration_f" "$cwd/$num_taxa.txt" $i $rep "$cwd/trees_without_bl_$num_taxa.txt"
pruned_net="without_$i.net"

nw_prune ../reference_rax_me_rooted.tree $i > backbone_rax_me.tree
nw_prune ../RAxML_result_rooted.REF $i > backbone_rax.tree


#python $spliting_path/ref_query_split.py ../aln.fa $i query.fa ref.fa

python $scripts/split_ref_query.py ref.fa $outgrp out.fa ref2.fa
mv ref2.fa ref.fa

echo "apple_running"
time run_apples.py -q query.fa -s ref.fa -t backbone_rax_me.tree -o placement_apple.jplace
guppy tog -o placement_apple.tree placement_apple.jplace


echo "q_$i" >> $out_apple
python  $scripts/compareNets.py ../true_net.net placement_apple.tree $i $rep 0 >> $out_apple
#python  $scripts/compareNets.py ../true_net.net ../reference_rax_me.tree 0 $rep >> $out_apple
python  $scripts/compareNets.py $pruned_net backbone_rax_me.tree 0 $rep 0 >> $out_apple

python $scripts/split_ref_query.py ../aln.fa $outgrp out.fa aln.fa

rm -r *.refpkg*

taxit create -l sim -P $i.refpkg_mafft --aln-fasta ref.fa --tree-stats ../RAxML_info.REF --tree-file backbone_rax.tree

echo "pplacer_running"
       
time pplacer -c $i.refpkg_mafft -j 6 aln.fa
mv aln.jplace placement_pplacer.jplace
guppy tog -o placement_pplacer.tree placement_pplacer.jplace 



echo "q_$i" >> $out_pplacer
python  $scripts/compareNets.py ../true_net.net placement_pplacer.tree $i $rep 0 >> $out_pplacer
#python  $scripts/compareNets.py ../true_net.net ../reference_rax.tree 0 $rep >> $out_pplacer
python  $scripts/compareNets.py $pruned_net backbone_rax.tree 0 $rep 0 >> $out_pplacer

rm -r *.refpkg_mafft
rm *.comp_nexus_output *.result *.jplace 
cd ..
done
rm RAxML_binaryModelParameters.REF RAxML_log.REF 
cd ..
cd ..

