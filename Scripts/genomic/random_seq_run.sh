#!/bin/bash

#rep=$1
num_taxa=$1
net_file=$2
migration_f=$3

cwd=`pwd`
for((rep=1;rep<=10;rep++))
do
out_apple="$cwd/rand_result_apple_$rep.txt"
out_pplacer="$cwd/rand_result_pplacer_$rep.txt"
out_epang="$cwd/rand_result_epang_$rep.txt"
out_sepp="$cwd/rand_result_sepp_$rep.txt"

rm $out_apple $out_pplacer $out_epang $out_sepp

#compare_path="/mnt/home/alaminmd/research/metagenomics/placement"
spliting_path="/mnt/home/alaminmd/research/scripts"
scripts="/mnt/home/alaminmd/research/scripts"

#tree_file="$num_taxa.txt"
#echo "$tree_file" 
#cd $taxa
cd $rep
#head -$rep ../$net_file|tail -1 > true_net.net  

#rm *.REF
#raxmlHPC-PTHREADS-AVX2 -f e -t reference_rax.tree -m GTRGAMMA -p 88 -n REF -s aln.fa -T 4
#raxmlHPC -f e -t reference_rax.tree -m GTRGAMMA -p 88 -n REF -s aln.fa
#python ~/research/scripts/fasta_phylip.py aln.fa aln.phy
#fastme -dJ -i aln.phy -u RAxML_result.REF -o reference_rax_me.tree -T 1


for ((i=1;i<=$num_taxa;i++))
do
cd $i


#python $scripts/network_drop_taxon.py "$cwd/$migration_f" "$cwd/$num_taxa.txt" $i $rep "$cwd/trees_without_bl_$num_taxa.txt"
pruned_net="without_$i.net"

#nw_prune ../reference_rax_me.tree $i > backbone_rax_me.tree
#nw_prune ../RAxML_result.REF $i > backbone_rax.tree


#python $spliting_path/ref_query_split.py ../aln.fa $i query.fa ref.fa

python $scripts/seqlen.py query.fa > temp.txt
while read len
do
python $scripts/create_random_seq.py $len $i
done < "temp.txt"

rm temp.txt

echo "apple_running"
time run_apples.py -q $i.fa -s ref.fa -t backbone_rax_me.tree -o placement_apple_2.jplace
guppy tog -o placement_apple_2.tree placement_apple_2.jplace

rm placement_apple_2.jplace


FILE="placement_apple_2.tree"
if test -f "$FILE"; then

echo "q_$i" >> $out_apple
python  $scripts/compareNets.py ../true_net.net placement_apple_2.tree $i $rep 0 >> $out_apple
#python  $scripts/compareNets.py ../true_net.net ../reference_rax_me.tree 0 $rep >> $out_apple
python  $scripts/compareNets.py $pruned_net backbone_rax_me.tree 0 $rep 0 >> $out_apple

fi

rm -r *.refpkg*

taxit create -l sim -P $i.refpkg_mafft --aln-fasta ref.fa --tree-stats ../RAxML_info.REF --tree-file backbone_rax.tree

echo "pplacer_running"

cat ref.fa $i.fa > aln_rand.fa       
time pplacer -c $i.refpkg_mafft -j 6 aln_rand.fa
mv aln_rand.jplace placement_pplacer_2.jplace
guppy tog -o placement_pplacer_2.tree placement_pplacer_2.jplace 


FILE="placement_pplacer_2.tree"
if test -f "$FILE"; then

echo "q_$i" >> $out_pplacer
python  $scripts/compareNets.py ../true_net.net placement_pplacer_2.tree $i $rep 0 >> $out_pplacer
#python  $scripts/compareNets.py ../true_net.net ../reference_rax.tree 0 $rep >> $out_pplacer
python  $scripts/compareNets.py $pruned_net backbone_rax.tree 0 $rep 0 >> $out_pplacer

fi

cd ..
done
cd ..
done
