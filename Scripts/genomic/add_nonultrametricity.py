from ete3 import Tree
import os,sys
import numpy as np
import random
import glob

file=sys.argv[1]
c=float(sys.argv[2])

low=-1*np.log(c)
high=np.log(c)

newf=file+'_m'
with open(file,'r') as inpf,open(newf,'w') as outf:
    for tree in inpf:
        if (tree=='\n'):
            continue
        
        t = Tree(tree, format=5)

        #print('Tree ..')
        #print (t.write(format=5))
        for node in t.traverse("postorder"):
            x=random.uniform(low,high)
            mult=np.exp(x)
            node.dist=node.dist*mult
        newtree=t.write(format=5)
        #print(newtree)
        outf.write('%s\n'%newtree)
