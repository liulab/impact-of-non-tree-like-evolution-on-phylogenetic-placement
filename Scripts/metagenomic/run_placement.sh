#!/bin/bash

rep=$1
num_taxa=$2
net_file=$3
migration_f=$4
cwd=`pwd`
out_apple1="$cwd/all_support_result_apple_$rep.txt"
out_pplacer1="$cwd/all_support_result_pplacer_$rep.txt"
out_apple2="$cwd/result_apple_$rep.txt"
out_pplacer2="$cwd/result_pplacer_$rep.txt"
out_epang="$cwd/result_epang_$rep.txt"
out_sepp="$cwd/result_sepp_$rep.txt"



rm $out_apple1 $out_apple2 $out_pplacer1 $out_pplacer2 $out_epang $out_sepp

#compare_path="/mnt/home/alaminmd/research/metagenomics/placement"
spliting_path="/mnt/home/alaminmd/research/scripts"
scripts="/mnt/home/alaminmd/research/scripts"


tree_file="$num_taxa.txt"
echo "$tree_file" 
#cd $taxa
cd $rep

head -$rep ../$net_file|tail -1 > true_net.net  

rm *.REF
raxmlHPC -f e -t reference_rax.tree -m GTRGAMMA -p 88 -n REF -s aln.fa
python ~/research/scripts/fasta_phylip.py aln.fa aln.phy
fastme -dJ -i aln.phy -u RAxML_result.REF -o reference_rax_me.tree -T 1

Rscript  $scripts/outgroup_rooting.R reference_rax_me.tree $num_taxa reference_rax_me_rooted.tree
Rscript  $scripts/outgroup_rooting.R RAxML_result.REF $num_taxa RAxML_result_rooted.REF

outgrp=`expr $num_taxa + 1`
#while read i
for ((i=1;i<=$num_taxa;i++))
do
cd $i

python $scripts/deep_network_drop_taxon.py "$cwd/$migration_f" "$cwd/$num_taxa.txt" $i $rep "$cwd/trees_without_bl_$num_taxa.txt"  
pruned_net="without_$i.net"

nw_prune ../reference_rax_me_rooted.tree $i > backbone_rax_me.tree
nw_prune ../RAxML_result_rooted.REF $i > backbone_rax.tree

python $scripts/split_ref_query.py ref.fa $outgrp out.fa ref2.fa
mv ref2.fa ref.fa

#cat NODE*>queries.fa
f=""
#count=0
c=0
for node in $(ls NODE*)
do
#count=`expr $count + 1`
#if [ $count == 4 ]
#then
#    break
#fi
f=$node

c=`expr $c + 1`

mafft --auto --addfragments $f --thread -1 ref.fa > all_aln_mult.fa
python ~/research/scripts/fasta_multiline_single.py all_aln_mult.fa all_aln.fa
rm all_aln_mult.fa

arrf=(${f//.fa/ })
q=${arrf[0]} 

#python $spliting_path/ref_query_split.py ../aln.fa $i query.fa ref.fa
python $scripts/split_ref_query.py all_aln.fa $q query_p.fa ref_p.fa 

python ~/research/scripts/replace_leafname.py query_p.fa $i

echo "apple_running"
time run_apples.py -q query_p.fa -s ref_p.fa -t backbone_rax_me.tree -o placement_apple.jplace
guppy tog -o placement_apple.tree placement_apple.jplace
cp placement_apple.tree "placement_apple.tree_$c"

rm -r *.refpkg*

taxit create -l sim -P $i.refpkg_mafft --aln-fasta ref_p.fa --tree-stats ../RAxML_info.REF --tree-file backbone_rax.tree

echo "pplacer_running"
       
time pplacer -c $i.refpkg_mafft all_aln.fa
mv all_aln.jplace placement_pplacer.jplace
guppy tog -o placement_pplacer.tree placement_pplacer.jplace 
cp placement_pplacer.tree "placement_pplacer.tree_$c"

#python $scripts/prune_placement_tree.py placement_apple.tree $f $num_taxa $i pruned_placement_apple.tree 
#python $scripts/prune_placement_tree.py placement_pplacer.tree $f $num_taxa $i pruned_placement_pplacer.tree 

echo "q_$i" >> $out_apple2
python  $scripts/compareNets.py ../true_net.net placement_apple.tree $i $rep 0 >> $out_apple2
#python  $scripts/compareNets.py ../true_net.net ../reference_rax_me.tree 0 $rep >> $out_apple2
python  $scripts/compareNets.py $pruned_net backbone_rax_me.tree 0 $rep 0 >> $out_apple2


echo "q_$i" >> $out_pplacer2
python  $scripts/compareNets.py ../true_net.net placement_pplacer.tree $i $rep 0 >> $out_pplacer2
#python  $scripts/compareNets.py ../true_net.net ../reference_rax.tree 0 $rep >> $out_pplacer2
python  $scripts/compareNets.py $pruned_net backbone_rax.tree 0 $rep 0 >> $out_pplacer2

#id="psup$i"
#rm *.$id

#raxmlHPC -m GTRCAT -p 12345 -f b -t placement_apple.tree -z ../RAxML_bootstrap.boot -n $id
#echo "q_$i" >> $out_apple1
#python3  $scripts/support_val_extraction.py RAxML_bipartitions.$id $i >> $out_apple1
#python3  $scripts/support_val_extraction.py RAxML_bipartitions.$id $i >> $out_apple1


done

rm -r *.refpkg_mafft
rm *.comp_nexus_output *.result *.jplace

cd ..
done
rm RAxML_binaryModelParameters.REF RAxML_log.REF
cd ..
cd ..

scontrol show job $SLURM_JOB_ID     ### write job information to SLURM output file.
js -j $SLURM_JOB_ID                 ### write resource usage to SLURM output file (powertools command).
